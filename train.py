from data import get_data_generators
from fall_model import get_model
from pathlib import Path
from tensorflow import data, TensorSpec
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, ReduceLROnPlateau
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.metrics import FalseNegatives, FalsePositives, TrueNegatives, TruePositives, CategoricalAccuracy
from tensorflow.keras.optimizers import Adam
from tensorflow_addons.metrics.matthews_correlation_coefficient import MatthewsCorrelationCoefficient as MCC

# Error: Cannot convert a symbolic Tensor to a numpy array
# This is solved by installing numpy version 1.19+

DATA_NAME = "sterile"
MODEL_NAME = "deep_lstm"
VERSION_NAME = "v0.0"
BATCH_SIZE = 32

results_root = Path(f"{DATA_NAME}-{MODEL_NAME}") / VERSION_NAME

TrainingDataGenerator, ValidationDataGenerator = get_data_generators(DATA_NAME)
training_data = data.Dataset.from_generator(TrainingDataGenerator,
                                            output_signature=(
                                                TensorSpec((BATCH_SIZE, 410, 3,)),
                                                TensorSpec((BATCH_SIZE, 8,))
                                            )).prefetch(BATCH_SIZE)
validation_data = data.Dataset.from_generator(ValidationDataGenerator,
                                              output_signature=(
                                                  TensorSpec((BATCH_SIZE, 410, 3,)),
                                                  TensorSpec((BATCH_SIZE, 8))
                                              )).prefetch(BATCH_SIZE)

model = get_model(MODEL_NAME)
model.compile(optimizer=Adam(learning_rate=1E-4),
              loss=CategoricalCrossentropy(reduction='sum'),
              metrics=[MCC(8),  # FalseNegatives(0.75), FalsePositives(0.75), TrueNegatives(0.75), TruePositives(0.75),
                       CategoricalAccuracy()])

# model.load_weights("pretrained_weights/weights.sterile-deep_lstm.004-20.3875560760-0.86375-0.88137.hdf5")

model.fit(x=training_data,
          y=None,
          epochs=10,
          batch_size=BATCH_SIZE,
          validation_data=validation_data,
          use_multiprocessing=True,
          workers=4,
          callbacks=[ModelCheckpoint(
              results_root / "weights" / "weights.{epoch:03d}-{val_loss:.10f}-{val_MatthewsCorrelationCoefficient:.5f}-"
                             "{val_categorical_accuracy:.5f}.hdf5",
              monitor='val_categorical_accuracy', save_best_only=False),
              TensorBoard(log_dir=results_root / "log",
                          histogram_freq=1,
                          write_graph=True,
                          update_freq=10,
                          # profile_batch='500,580'
                          ),
              ReduceLROnPlateau(monitor='val_loss',
                                mode='min',
                                factor=0.1,
                                patience=3,
                                verbose=1,
                                min_delta=0.001,
                                min_lr=1E-26)])

model.summary()
