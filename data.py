from generator import DataGenerator


def get_data_generators(by_name="latest"):
    if "sterile" == by_name:
        return sterile_generators()
    elif "smooth" == by_name:
        return smooth_generators()
    elif "varied_normal" == by_name:
        return varied_normal_generators()
    elif "varied_smooth" == by_name:
        return varied_normal_and_smooth_generators()
    elif "varied_smooth_noisy" == by_name:
        return varied_smooth_noisy_generators()
    elif "latest" == by_name:
        return varied_smooth_noisy_generators()
    elif "test" == by_name:
        return test_generators()

    raise ValueError("There are no data generators associated with the provided name: " + by_name + ".\n")


def sterile_generators():
    class TrainingDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0, 6.258641617, 256),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.02, 0.04),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0, 1, 32),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(1, 1, 1),
                             smoothing_timepoints_init=(0, 0, 1))

    class ValidationDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0.5, 6, 19),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0.01, 0.99, 77),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(1, 1, 1),
                             smoothing_timepoints_init=(0, 0, 1))

    return TrainingDataGenerator, ValidationDataGenerator


def test_generators():
    class TrainingDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0, 6.258641617, 32),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.02, 0.04),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0, 1, 32),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(0.905, 1.095, 5),
                             smoothing_timepoints_init=(0, 0, 1))

    class ValidationDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0.5, 6, 17),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0.01, 0.99, 19),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(0.905, 1.095, 6),
                             smoothing_timepoints_init=(0, 0, 1))

    return TrainingDataGenerator, ValidationDataGenerator


def smooth_generators():
    class TrainingDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0, 6.258641617, 256),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.02, 0.04),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0, 1, 32),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(1, 1, 1),
                             smoothing_timepoints_init=(4, 4, 1))

    class ValidationDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0.5, 6, 19),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0.01, 0.99, 77),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(1, 1, 1),
                             smoothing_timepoints_init=(4, 4, 1))

    return TrainingDataGenerator, ValidationDataGenerator


def varied_normal_generators():
    class TrainingDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0, 6.258641617, 256),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.02, 0.04),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0, 1, 32),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(0.91, 1.09, 5),
                             smoothing_timepoints_init=(0, 0, 1))

    class ValidationDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0.5, 6, 19),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0.01, 0.99, 77),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(0.91, 1.09, 5),
                             smoothing_timepoints_init=(0, 0, 1))

    return TrainingDataGenerator, ValidationDataGenerator


def varied_normal_and_smooth_generators():
    class TrainingDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0, 6.258641617, 256),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.02, 0.04),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0, 1, 32),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(0.91, 1.09, 5),
                             smoothing_timepoints_init=(4, 4, 1))

    class ValidationDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0.5, 6, 19),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0.01, 0.99, 77),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0,
                             non_fault_amplitudes_init=(0.91, 1.09, 5),
                             smoothing_timepoints_init=(4, 4, 1))

    return TrainingDataGenerator, ValidationDataGenerator


def varied_smooth_noisy_generators():
    class TrainingDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0, 6.258641617, 64),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.02, 0.04),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0, 1, 32),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0.025,
                             non_fault_amplitudes_init=(0.905, 1.095, 16),
                             smoothing_timepoints_init=(4, 4, 1))

    class ValidationDataGenerator(DataGenerator):
        def __init__(self):
            super().__init__(phases_init=(0.5, 6, 19),
                             signal_time_init=(0.1, 0.1, 1),
                             fault_durations_init=(0.01, 0.02, 0.03, 0.04, 0.05, 0.06),
                             sampling_frequencies_init=4096,
                             signal_frequencies_init=(50, 50, 1),
                             severities_init=(0.01, 0.99, 77),
                             rms_init=230,
                             fault_start_timepoint_init=0.04,
                             noise_percent_init=0.025,
                             non_fault_amplitudes_init=(0.905, 1.095, 32),
                             smoothing_timepoints_init=(4, 4, 1))

    return TrainingDataGenerator, ValidationDataGenerator
